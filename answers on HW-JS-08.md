# Questions and answers for HW-JS-08:

1. Опишіть своїми словами, що таке Document Object Model (DOM)

   Document Object Model (DOM) – об'єктна модель документа. "Сімейна" структура всього HTML документа, у вигляді "дерева". Тобто, представляє увесь зміст сторінки в якості змінних об'єктів.

   DOM відповідає за структуру та вигляд самої сторінки (document). BOM (об'єктна модель браузера) відповідає за структуру з боку браузера. SCCOM відповідає за опис стилів CSS-файлів. Взаємодія між ними відбувається за допомогою JS.

   DOM, BOM, CSSOM є частинами загальної специфікації HTML.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

   Обидві властивості працюють із внутрішнім змістом HTML. Відмінність полягає у тому, що innerHTML працює з усім контентом, включаючи всі теги, в які цей контент обгорнутий. А innerText – безпосередньо лише з самим текстом.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

   Звернутися до елемента сторінки можна декількома способами:
   I. За допомогою DOM-навігації.

      1). За допомогою базової DOM-навігації:
            documentElement, body чи heder:
               <html> = document.documentElement
               <head> = document.head
               <body> = document.body
               
      2). За допомогою навігації тільки по елементам:
            • Children - колекція дітей, які є елементами;
            • firstElementChild, lastElementChild – перший та останній дочірній елемент;
            • previousElementSibling, nextElementSibling - сусіди-елементи;
            • parentElement – батько-елемент.

      3). За допомогою додаткових специфічних властивостей типу, наприклад таблиці (table):
            • table.rows - ​​колекція рядків <tr>;
            • table.caption/tHead/tFoot – посилання на елементи таблиці <caption>, <thead>, <tfoot>;
            • table.tBodies – колекція елементів таблиці <tbody> (за специфікацією їх може бути більше одного).

   Цей засіб зручний, коли елементи стоять поруч чи ідуть один за одним, або коли необхідно звернутися до першого чи останнього елементу.

   II. За допомогою пошуку елементів: getElement, querySelector.

      1). getElementBy – трохи застарілі методи:
         • getElementById(id) – пошук по ID;
            ** Є глобальна змінна з ім'ям, вказаним у id: element.style.backgroundColor...;
         • elem.getElementsByTagName(tag) – шукає елементи з цим тегом та повертає їхню колекцію;
         • elem.getElementsByClassName(className) – повертає елементи, які мають цей CSS-клас;
         • document.getElementsByName(name) повертає елементи із заданим атрибутом name (дуже рідко використовується).

      2). querySelector – більш короткі, універсальні та сучасні методи:
         • querySelector() – повертає перший елемент;
         • querySelectorAll() – повертає всі елементи всередині. Працює з forEach.

      Висновок: querySelector вважається сучасним, універсальним та більш вживаним засобом.
         
